package org.aossie.agoraandroid.data.db.model

data class Score(
  val numerator: String? = null,
  val denominator: String? = null
)